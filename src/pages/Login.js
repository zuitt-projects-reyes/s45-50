import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button} from 'react-bootstrap';
import UserContext from '../UserContext'

export default function Login(props){

	// allows us to consume the User context object and its properties to use for user validation
	const {user, setUser} = useContext(UserContext);
	console.log(user)

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(true);

	function authenticate(e){
		e.preventDefault()

		// set the email of the authenticated user in the local storage

		// Syntax: localStorage.setItem("key", value)
		localStorage.setItem("email", email)

		// to access the user information, it can be done using localStorage, this is necessary to update the user state which will help update the App component and rerender it to avoid refreshing the page upon user login and logout

		// when state change components are rerendered and the AppNavbar component will be updated based on the user credentials
		setUser({
			email: localStorage.getItem('email')
		})

		setEmail('')
		setPassword('')

		alert(`${email} has been verified, welcome back!`)
	}

	useEffect(() => {
		if(email !== '' && password !== ''){
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password])

	return(
		
		(user.email !== null) ?
		
		<Navigate to= "/courses"/>
		
		:

		<Form onSubmit={e => authenticate(e)}>
		<h1>Log In</h1>
			<Form.Group controlId="email">
				<Form.Label>Email Address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Insert your registered email here"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Insert your registered password here"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{ isActive ?
				<Button variant="primary" type="submit" id="submitBtn" className="mt-3 mb-3">
					Log In
				</Button>
				:
				<Button variant="danger" type="submit" id="submitBtn" className="mt-3 mb-3" disabled>
					Log In
				</Button>
			}
		</Form>

	)
}