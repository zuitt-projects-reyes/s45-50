import {useState, useEffect} from 'react';
import {Card, Button} from 'react-bootstrap';

export default function CourseCard({courseProp}) {

    console.log(courseProp);
        // expected result is coursesData[0]
    console.log(typeof courseProp);
        // result: object

    const [count, setCount] = useState(0);
    // syntax: const [getter, setter] = useState(initialValueOfGetter)
    const [seats, setSeats] = useState(30);
  

    function enroll(){
        if (seats > 0) {
            setCount(count + 1);
            console.log('Enrollees: ' + count);
            setSeats(seats - 1);
            console.log('Seats: ' + seats);
        }/* else {
            alert(`No more seats available for ${name}`);
        };*/
    }

    useEffect(() => {
        if(seats === 0){
            alert("No more seats available")
        }

    }, [seats])
    // syntax: useEffect(() => {}, [optionalParameter])


    const {name, description, price} = courseProp

    return (
        <Card>
            <Card.Body>
                <Card.Title>{name}</Card.Title>
                <Card.Subtitle>Description:</Card.Subtitle>
                <Card.Text>{description}</Card.Text>
                <Card.Subtitle>Price:</Card.Subtitle>
                <Card.Text>{price}</Card.Text>
                <Card.Text>Enrollees: {count}</Card.Text>
                <Card.Text>Seats: {seats}</Card.Text>
                <Button variant="primary" onClick={enroll}>Enroll</Button>
            </Card.Body>
        </Card>
    )
}
