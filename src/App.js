import { useState } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
// v5 of react-router-dom: Routes is Switch
import AppNavBar from './components/AppNavbar';
// import Banner from './components/Banner';
// import Highlights from './components/Highlights'
import Home from './pages/Home';
import Courses from './pages/Courses';
import Error from './pages/Error';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Register from './pages/Register';
import './App.css';
import { UserProvider } from './UserContext'

function App() {

  const [user, setUser] = useState({ email: localStorage.getItem('email') })


  const unsetUser = () => {
    localStorage.clear();
  }


  return (
    <UserProvider value={{ user, setUser, unsetUser }}>
      <Router>
        <AppNavBar />
        <Container>
          <Routes>
            {/*  <Banner/>
          <Highlights/>*/}
            <Route exact path="/" element={<Home />} />

            {/*v5 routing: <Route exact path= "/" component= {Home}/>*/}

            <Route exact path="/courses" element={<Courses />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/logout" element={<Logout />} />
            <Route exact path="*" element={<Error />} />
          </Routes>
        </Container>
      </Router>
    </UserProvider>
  )
}

export default App;
